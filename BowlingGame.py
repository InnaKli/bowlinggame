# from unittest import result


class BowlingGame:
    def __init__(self):
        self.rolls = []
#function created to set number of pins in a roll
    def roll(self, pins):
        self.rolls.append(pins)
#function created to calculate the game score depending on number of frames and pins scored
    def score(self, roll_index):
        result = 0
        for roll_index in range(10):
            if roll_index in range(10):
                result += self.score(roll_index)
                roll_index += 1
            elif self.is_spare(roll_index):
                result += self.spare_score(roll_index)
                roll_index += 2
            else:
                result += self.frame_score(roll_index)
            roll_index += 2
            return result
#function created to intialise a stirke if 10 pins scored in one roll
    def is_strike(self, roll_index):
        return self.rolls[roll_index] == 10
#function created to intialize a spare if total of 10 pins scored in 2 rolls in one frame
    def is_spare(self, roll_index):
        return self.rolls[roll_index] + self.rolls[roll_index + 1] == 10
#function created to add scores of two frames if strike is scored
    def strike_score(self, roll_index):
        return 10 + self.rolls[roll_index + 1] + self.rolls[roll_index + 2]
#function ceated to add score of one consecutive frame if a spare is scored
    def spare_score(self, roll_index):
        return 10 + self.rolls[roll_index + 2]
#function created to calculate the score of a single frame
    def frame_score(self, roll_index):
        return self.rolls[roll_index] + self.rolls[roll_index + 1]
