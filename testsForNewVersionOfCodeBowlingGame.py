
class Bowling_Game(object):
    def __init__(self):
        self._frames = []

    def _current_frame(self):
        if not self._frames:
            return self.new_frame()
        if len(self._frames) == 10:
            return self._frames[-1]
        if self._frames[-1].is_complete():
            return self.new_frame()
        return self._frames[-1]

    def new_frame(self):
        _new = Frame()
        self._frames.append(_new)
        return _new

    def score(self):
        return sum([frame.score() for frame in self._frames])

    def roll(self, count):
        current_frame = self._current_frame()
        for frame in self._frames:
            frame.add_roll(count)


class Frame(object):
    def __init__(self):
        self._pins = []
        self._score = 0
        self._extra_scoring_balls = 0

    def add_roll(self, roll):
        if not self.is_complete():
            self.score_roll(roll)
            self._pins.append(roll)
            if self.is_complete():
                # Frame is completed this roll
                if self._is_strike():
                    self._extra_scoring_balls = 2
                elif self._is_spare():
                    self._extra_scoring_balls = 1

        else:
            self.score_roll(roll)

    def score(self):
        return self._score

    def score_roll(self, roll):
        if self.is_complete():
            if not self._extra_scoring_balls:
                return
            self._score += roll
            self._extra_scoring_balls -= 1
        else:
            self._score += roll

    def _is_spare(self):
        return not self._is_strike() and sum(self._pins) == 10

    def _is_strike(self):
        return self._pins == [10]

    def is_complete(self):
        return self._is_spare() or self._is_strike() or len(self._pins) == 2

import unittest
import newVersionOfCodeforBowlingGame

# frame, roll, score, total_score
'''
text_data = """
1   1   1   
1   2   4   5
2   1   4   
2   2   5   14
3   1   6   
3   2   4   29
4   1   5   
4   2   5   49
5   1   10  
5   2       60
6   1   0   
6   2   1   61
7   1   7   
7   2   3   77
8   1   6   
8   2   4   97
9   1   10  
9   2       117
10  1   2   
10  2   8   
10  3   6   133
'''

rolls = [
    1,
    4,
    4,
    5,
    6,
    4,
    5,
    5,
    10,
    0,
    1,
    7,
    3,
    6,
    4,
    10,
    2,
    8,
    6,
]


class Tests(unittest.TestCase):
    def setUp(self):
        self.game = Bowling_Game()

    def do_rolls(self, n):
        for i in rolls[:n]:
            self.game.roll(i)

    def test_no_rolls(self):
        self.assertEqual(0, self.game.score())

    def test_one_roll(self):
        self.do_rolls(1)
        self.assertEqual(1, self.game.score())

    def test_two_rolls(self):
        self.do_rolls(2)
        self.assertEqual(5, self.game.score())

    def test_four_rolls(self):
        self.do_rolls(4)
        self.assertEqual(14, self.game.score())

    def test_rolls_including_spare(self):
        self.do_rolls(7)
        self.assertEqual(34, self.game.score())

    def test_full_game(self):
        self.do_rolls(len(rolls))
        self.assertEqual(133, self.game.score())

    def test_perfect_game(self):
        for i in range(22):
            self.game.roll(10)
        self.assertEqual(300, self.game.score())